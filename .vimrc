" Basic setup: disable vi bindings and set encoding.
set nocompatible
set encoding=utf-8
filetype off

" Set the indentation to four spaces - tabs are expanded.
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

" Enable automatic indenting
filetype plugin indent on

" Display whitespace characters.
set list
set listchars=tab:•\ ,trail:•,extends:»,precedes:«

" Display line numbers, remove tildes.
set number
highlight LineNr ctermfg=darkgrey
highlight EndOfBuffer ctermfg=black

" Enable syntax highlighting with terminal colours.
set t_Co=16
syntax enable

" Allow buffer switch without save.
set hidden
